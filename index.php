<?php

require "./animal.php";
require "./ape.php";
require "./frog.php";

echo "<h3>Release 0</h3>";

$sheep = new Animal("shaun");

echo $sheep->get_name() . "<br>";
echo $sheep->get_legs() . "<br>";
echo $sheep->get_cold_blooded() . "<br>";

echo "<br>";

echo "<h3>Release 1</h3>";

$sungokong = new Ape("kera sakti");
$sungokong->yell();

echo "<br>";

$kodok = new Frog("buduk");
$kodok->jump();